class CoursesController < ApplicationController
  def index
    @courses = CareerFoundry::APIClient.get_all_courses
  end

  def show
    country_code = Geolocation.get_country_code
    @course_detail = CareerFoundry::APIClient.get_course_by_slug_and_location(params[:slug], country_code)
  end
end
