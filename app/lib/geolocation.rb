require 'json'

class Geolocation
  GEOIP_API_URL = 'http://freegeoip.net/json/'

  def self.get_country_code
    response_body = HTTParty.get(GEOIP_API_URL).body
    JSON.parse(response_body)["country_code"]
  end

end
