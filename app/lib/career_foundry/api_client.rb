require 'json'

module CareerFoundry
  class APIClient
    API_URLS = {
      all: "https://careerfoundry.com/en/api/courses/",
      course: "https://careerfoundry.com/en/api/courses/{course_slug}"
    }

    def self.get_all_courses
      api_url = API_URLS[:all]
      resource = get_resource(api_url)
      parse_to_json_response(resource)
    end

    def self.get_course_by_slug_and_location(slug, country_code)
      api_url = API_URLS[:course].gsub("{course_slug}", slug)
      resource = get_resource(api_url)
      response = parse_to_json_response(resource)
      transform_response_for_location(response, country_code)
    end

    private

    def self.transform_response_for_location(response, country_code)
      response["price"] = if response["price"].keys.include?(country_code)
        response["price"][country_code]
      else
        response["price"]["NA"]
      end
      response
    end

    def self.parse_to_json_response(resource)
      JSON.parse(resource)
    end

    def self.get_resource(url)
      HTTParty.get(url).body
    end
  end
end
