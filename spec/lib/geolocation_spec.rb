require 'spec_helper'

RSpec.describe Geolocation do
  it "returns country code" do
    country_code = Geolocation.get_country_code
    expect(country_code).to be not_empty
  end
end
