require 'spec_helper'

RSpec.describe CareerFoundry::APIClient do
  it "gets all courses" do
    api_response = CareerFoundry::APIClient.get_all_courses

    expect(api_response.class).to be(Hash)
    expect(api_response.key).to include("courses")
    expect(api_response["courses"].count).to be > 0
  end

  it "gets detail of one course" do
    course_slug = 'become-a-ui-designer'
    code_country = "UK"
    api_response = CareerFoundry::APIClient.get_course_by_slug_and_location(course_slug, code_country)

    expect(api_response.class).to be(Hash)
    expect(api_response["price"]).to match(/^£\d+$/)
  end
end
